const VueLoaderPlugin = require('vue-loader/lib/plugin')
const EsmWebpackPlugin = require("@purtuga/esm-webpack-plugin");

module.exports = {
  mode: 'development',
  output: {
    filename: 'bundle.js',
    libraryTarget: 'var',
    library: 'renderEngine'
    //library: 'Vitrine'
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      // this will apply to both plain `.js` files
      // AND `<script>` blocks in `.vue` files
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  plugins: [
    // make sure to include the plugin!
    new VueLoaderPlugin(),
    new EsmWebpackPlugin(),
  ]
};