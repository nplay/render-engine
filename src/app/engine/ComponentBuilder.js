import PropsAddon from "./addon/Props"
import StyleAddon from "./addon/Style"
import Component from "./component"
import SwiperParamsAddon from "./addon/SwiperParams"

class VueComponentConstructor
{
    constructor()
    {
        this.data = {}
        this.template = null
    }

    /**
     * 
     * @param {Object} data 
     */
    async assignData(data){
        this.data = Object.assign(this.data, data)
        return this
    }

    /**
     * 
     * @param {Object} data 
     * @returns {VueComponentConstructor}
     */
    async setTemplate(data){
        this.template = data
        return this
    }
}

class ComponentBuilder
{
    constructor()
    {
        this.vueComponentConstructor = new VueComponentConstructor()
    }

    async buildAddons()
    {
        await this.buildPropsAddon()
        await this.buildSwiperParamsAddon()
        await this.buildStyleAddon()
    }

    /**
     * @returns {ComponentBuilder}
     */
    async buildPropsAddon()
    {
        let output = await new PropsAddon(this.vueComponentConstructor.template, this.vueComponentConstructor.data).get()

        if(!output)
            return this

        await this.vueComponentConstructor.setTemplate(output.document)
        this.vueComponentConstructor = Object.assign(this.vueComponentConstructor, output.props)

        return this
    }

    /**
     * @returns {ComponentBuilder}
     */
    async buildSwiperParamsAddon()
    {
        let output = await new SwiperParamsAddon(this.vueComponentConstructor.template, this.vueComponentConstructor.data).get()

        if(!output)
            return this

        await this.vueComponentConstructor.setTemplate(output.document)

        this.vueComponentConstructor.data.swiper = this.vueComponentConstructor.data.swiper || {}
        this.vueComponentConstructor.data.swiper = Object.assign(this.vueComponentConstructor.data.swiper, output.props)

        return this
    }

    /**
     * @returns {ComponentBuilder}
     */
    async buildStyleAddon()
    {
        let output = await new StyleAddon(this.vueComponentConstructor.template, this.vueComponentConstructor.data).get()

        if(!output)
            return this

        await this.vueComponentConstructor.setTemplate(output.document)
        await this.vueComponentConstructor.assignData({style: output.style})

        let params = Object.assign(options, {
            data: () => { return data },
            template: document
        })

        this._component = new Component(params)
        return this._component
    }

    /**
     * 
     * @param {String} document Representation of document.body codified element 
     * @param {Object} data 
     * @returns {Component}
     */
    async getResult(document, data)
    {
        let template = await this.decode(document)
        
        await this.vueComponentConstructor.setTemplate(template)
        await this.vueComponentConstructor.assignData(data)

        await this.buildAddons()
        
        return new Component(this.vueComponentConstructor)
    }

    /**
     * 
     * @param {String} string encodeURIComponent with base64
     * @returns {String} string decoded
     */
    decode(string)
    {
        let decoded = window.atob(string)
        return decodeURIComponent(decoded)
    }
}

export {
    ComponentBuilder,
    VueComponentConstructor
}