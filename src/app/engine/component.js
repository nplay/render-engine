
import Vue from 'vue'
import dinamic from '../../components/dinamic.vue'
import {VueComponentConstructor} from './ComponentBuilder'

class Component
{
    /**
     * 
     * @param {VueComponentConstructor} params 
     */
    constructor(params)
    {
        /**
         * events: beforeRender, afterRender
         */
        this.events = new EventTarget
        this._dinamicComponent = null

        this.params = params
        this.options = options
    }

    /**
     * @returns {dinamic}
     */
    get get()
    {
        if(this._dinamicComponent == null)
        {
            let ComponentClass = Vue.extend(dinamic)
            this._dinamicComponent = new ComponentClass(this.params)
        }

        return this._dinamicComponent
    }

    mount()
    {
        let event = new CustomEvent('mountBefore', {
            detail: {
              target: this.get
            }
        })
        this.events.dispatchEvent(event)

        this.get.$mount();

        event = new CustomEvent('mountAfter', {
            detail: {
              target: this.get
            }
        })
        this.events.dispatchEvent(event)
    }
}

export default Component