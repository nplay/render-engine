
import Mustache from 'mustache'

class StyleAddon
{
    constructor(document, data, options = {removeTags: true})
    {
        this._data = data

        this.options = options
        this.document = document
    }

    async _process(string, data)
    {
        this.document = await this._parseQuoteTags(string, data)
        this.styles = await this._tags(this.document)
    }

    async _parseQuoteTags(string, data)
    {
        //Disable global escape
        let defaultTags = Mustache.tags

        Mustache.tags = ['[', ']']
        Mustache.escape = function(text) {return text;}
        string = Mustache.render(string, data)
        Mustache.tags = defaultTags

        return string
    }

    async _tags(string)
    {
        let parser = new DOMParser();
        let body = parser.parseFromString(string, 'text/html').body
        body.firstChild.setAttribute('id', `_${this._data.uid}`)
        let styles = body.querySelectorAll('style')

        let stylesHTML = new Array
  
        for (var i = 0; i < styles.length; ++i)
        {
           let node = styles[i];
           stylesHTML.push(node.innerHTML)
           node.remove()
        }
  
        if(this.options.removeTags)
           this.document = body.innerHTML;

        return stylesHTML.join('\n')
    }

    async get()
    {
        await this._process(this.document, this._data)

        if(this.styles.length == 0)
            return null

        return {
            style: this.styles,
            document: this.document
        }
    }
}

export default StyleAddon