
class PropsAddon
{
    constructor(document, data, options = {})
    {
        this._data = data
        this._document = document

        this.options = options
    }

    async _process(string, data)
    {
        let parser = new DOMParser();
        let body = parser.parseFromString(string, 'text/html').body
        let node = body.querySelector('script[data-context="component"]')
        let rawData = null

        if(!node)
            return null

        let fn = new Function(node.innerHTML)
        rawData = fn()
        node.remove()

        return {
            document: body.innerHTML,
            props: rawData
        }
    }

    async get()
    {
        return  await this._process(this._document, this._data)
    }
}

export default PropsAddon