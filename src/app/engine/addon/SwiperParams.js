
/**
 * Additional configuration
 */
class SwiperParamsAddon
{
    constructor(document, data)
    {
        this._data = data
        this.document = document
    }

    async get()
    {
        let parser = new DOMParser();
        let body = parser.parseFromString(this.document, 'text/html').body
        let node = body.querySelector('script[data-context="swiper-params"]')
        let rawData = null

        if(!node)
            return rawData

        let fn = new Function(node.innerHTML)
        rawData = fn()
        node.remove()
  
        return {
            document: body.innerHTML,
            props: rawData
        }
    }
}

export default SwiperParamsAddon