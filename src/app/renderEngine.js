
import Component from './engine/component'
import { ComponentBuilder } from './engine/ComponentBuilder'

class RenderEngine
{
    constructor(document = '', data = new Object)
    {
        this._data = data
        this._document = document

        this._component = null
    }

    async component()
    {
        let component = this.buildComponent()

        return component
    }

    /**
     * @returns {Component}
     */
    async buildComponent()
    {
        if(this._component)
            return this._component

        let componentBuilder = new ComponentBuilder()
        this._component = await componentBuilder.getResult(this._document, this._data)
        return this._component
    }
}

export default RenderEngine